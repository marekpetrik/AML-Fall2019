%% initialization
pos2pix = @(xp,yp,xn,yn) [floor(xp*xn) floor(yp*yn)];
Size=100;
rewards = zeros(Size,Size);
Q=rewards;
%% create rewards
y=linspace(.2,.8,Size);
x = .3*sin(10*y)+y*.1+.4;
for g =1:length(x)
    inds = pos2pix(x(g),y(g),Size,Size);
    rewards(inds(1),inds(2))=1;
end
B = (1./(abs([-3:3])+1))'*(1./(abs([-3:3])+1));
rewards = (conv2(rewards,B,'same'));
B = (1./(abs([-5:5])+1))'*(1./(abs([-5:5])+1));
rewards = conv2(rewards,B,'same');
rewards= log(rewards+1);
figure(1)
surf(rewards)
%% generate episodes
MG=max(max(rewards))*1.01;
gamma=1;
alpha=.3;
 for e=1:20
st = pos2pix(x(Size),y(Size),Size,Size);
 for episode=1:10000
    fail=0;
    goal=0;
    indx=floor(rand(1,1)*Size)+1;
    indy=floor(rand(1,1)*Size)+1;
    s =  [indx indy];
    t=0;
while ~fail && ~goal &&t<10*Size
    [sp,fail]= policy(s,Q);
    if sp==st
       R=1;
       goal=1;
       fail=0;
    else
        R = 10*(rewards(s(1),s(2))-MG)./MG;
        if (sp(1)~=s(1)) && (sp(2)~=s(2))
            R=R*sqrt(2); 
        end
    end
    if ~fail
        Q(s(1),s(2)) = Q(s(1),s(2))+alpha*(R+gamma*Q(sp(1),sp(2))-Q(s(1),s(2))); 
    s=sp;
    else
        Q(s(1),s(2)) = Q(s(1),s(2))*1.1-1;
    end
    s;
    t=t+1;
end
episode
 end
 figure(2)
 surf(Q(2:end-1,2:end-1))
 end
save('Q','Q')
  %% make paths
 load('Q')
 figure(3)
 contour(1:Size,1:Size,rewards)
 for x=2:10:Size-1
     for y=2:10:Size-1
        fail=0;
        goal=0;
        indx=x;
        indy=y;
        s =  [indx indy];
        X=[indx];Y=[indy];
        t=0;
            while ~fail && ~goal &&t<10*Size
                [sp,fail]= policy(s,Q);
                s=sp;
                X=[X;s(1)];
                Y=[Y;s(2)];
                t=t+1;
            end
        figure(3);hold on
        plot(Y,X,'color','b')
     end
 end