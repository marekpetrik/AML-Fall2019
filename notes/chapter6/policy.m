function [s,fail] = policy(s,Q)
    fail=0;
    try
        % im too lazy to check the indices bounds, so try catch it is (Sorry Francesco)
        up = Q(s(1),s(2)+1);
        down = Q(s(1),s(2)-1);
        left = Q(s(1)-1,s(2));
        right = Q(s(1)+1,s(2));
        upright = Q(s(1)+1,s(2)+1);
        downright = Q(s(1)+1,s(2)-1);
        upleft = Q(s(1)-1,s(2)+1);
        downleft = Q(s(1)-1,s(2)-1);
        A = find([up down left right upright downright upleft downleft]==max([up down left right upright downright upleft downleft]),1);
        switch A
            case 1
                sp =[s(1),s(2)+1]; 
            case 2
                sp = [s(1),s(2)-1];
            case 3
                sp = [s(1)-1,s(2)];  
            case 4
                sp = [s(1)+1,s(2)];
            case 5 
                sp = [s(1)+1,s(2)+1];
            case 6
                sp = [s(1)+1,s(2)-1];
            case 7
                sp = [s(1)-1,s(2)+1];
            case 8
                sp = [s(1)-1,s(2)-1];
        end
        s=sp;
     catch
        fail=1;
     end
end

