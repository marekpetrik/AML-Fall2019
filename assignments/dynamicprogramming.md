# Dynamic Programming Assignment

This assignment covers more-advanced material from the book: Dynamic Programming by Puterman (any edition). Please see Chaper 6 in this book (MDP on the website). You may need to have a look at other chapters too to make sense of this one.

## Problem 1

Consider and MDP with a single action $`a`$. Let $`P`$ be the matrix of transition probabilities and let $`r`$ be the vector of rewards. Here, $`P_{ij}`$ represents the probability of transitioning from a state i to a state j. Suppose that the goal is to compute the value function for the $`\gamma`$-discounted infnite-horizon objective. Show that the value function $`v`$ can be computed as follows:

```math 
v = (I-\gamma P)^{-1} r~, 
```

where $`I`$ is the identity matrix. You may want to review geometric series and read up on the Neumann series.

## Problem 2

Show that the occupancy frequency $`u`$ of the MDP in Problem 1 can be computed as follows:

```math 
u = (I-\gamma P^T)^{-1} p_0 ,
```

where $`p_0`$ is the initial distribution.

## Problem 3 (if you are comfortable with LPs)

Formulate the linear programs that can be used to solve for the optimal value function of an MDP.What is its dual? What is the relationship between the value function and the occupancy frequency? How are they related to the return of the MDP?
