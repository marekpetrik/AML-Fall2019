# CS950: Advanced Machine Learning #

### Where and When ###

- Kingsbury N233
- Tue/Thu  3:40pm - 5:00pm

# What #

This seminar will cover *reinforcement learning*. The goal in reinforcement learning is to learn how to act while interacting with a dynamic and complex environment. Instead of trying to cover all reinforcement learning topics, we will focus on the foundations needed to understand other concepts.

1. Markov decision processes: Policy iteration, Value iteration, Linear programming
- Value Function Approximation: LSTD, LSPI, ALP
- Direct policy search
- Uncertainty: Bandits and Adversarial Robustness

We will cover relevant topics from linear algebra, mathematical optimization, and statistics as needed.

# Schedule

The schedule is subject to change based on our progress and interests.

| Date   | Day  | Topic                               | Presenter              | Reading|
| ------ | ---- | ----------------------------------- | ---------------------- | ------ |
| Aug 27 | Tue  | Introduction                        | Marek                  |        |
| Aug 29 | Thu  | Multi-armed Bandits                 | Marek                  | RL 2   |
| Sep 03 | Tue  | Markov Decision Processes           | Marek                  | RL 3   |
| Sep 05 | Thu  | Dynamic Programming I               | Deepak,Jason,Monkie    | RL 4   |
| Sep 10 | Tue  | (*) Dynamic Programming II          | Marek                  | MDP 6  |
| Sep 12 | Thu  | Project presentations               | **Project**            |        |
| Sep 17 | Tue  | Monte-Carlo Methods                 | Francesco,Devin,Coral  | RL 5   |
| Sep 19 | Thu  | TD Learning                         | Paul,Marek,Rasel       | RL 6   |
| Sep 24 | Tue  | Review                              | Marek                  |        |
| Sep 26 | Thu  | Model-based Methods                 | Jiahao,Xihong,Lynette  | RL 8   |
| Oct 01 | Tue  | On-Policy Approximation I           | Kristian,Lynette,Deepak| RL 9   |
| Oct 03 | Thu  | On-Policy Approximation II          | Marek                  |        |
| Oct 08 | Tue  | Off-Policy Control and Approximation| Sepideh                | RL 10  |
| Oct 10 | Thu  | Off-Policy Control and Approximation| Marek                  | RL 11  |
| Oct 15 | Tue  | *Virtual Monday*                    | *No class*             |        |
| Oct 17 | Thu  | Approximate LP, Policy Gradient     | Marek                  | RL 13  |
| Oct 22 | Tue  | Robust RL                           | Marek                  |        |
| Oct 24 | Thu  | Robust RL                           | Marek                  |        |
| Oct 29 | Tue  | Project updates                     | **Project**            |        |
| Oct 31 | Thu  | Project updates                     | **Project**            |        |
| Nov 05 | Tue  | Tax Collection Optimization         | Sepideh, Deepak        |        |
| Nov 07 | Thu  | Model Predictive Control            | Paul, Francesco        |        |
| Nov 12 | Tue  | Thompson Sampling                   | Kristian, Rasel        |        |
| Nov 14 | Thu  | Optimizer's Curse                   | Monkie, Lynette        |        |
| Nov 19 | Tue  | Biasing with Discount               | Sepideh, Jiahao        |        |
| Nov 21 | Thu  | RL for Recommendations              | Xiangu, Xihong         |        |
| Nov 26 | Tue  | Bayesian RL in POMDPs               | Jason, Devin           |        |
| Nov 28 | Thu  | *Thanksgiving*                      |                        |        |
| Dec 03 | Tue  | *No class*                          | Student                |        |
| Dec 05 | Thu  | Project presentations (3:40-6:40)   | **Project**            |        |

## Papers ##

|Paper                                    |Presenters (critic,defender)|
|:----------------------------------------|:---------------------------|
| Tax Collection Optimization             | Sepideh, Deepak            |
| Model Predictive Control and RL         | Paul, Francesco            |
| Thompson Sampling                       | Kristian, Rasel            |
| Optimizer's Curse                       | Monkie, Lynette            |
| Biasing with a Discount Factor          | Sepideh, Jiahao            |
| RL for Recommendations                  | Xiangyu, Xihong            |
| Bayesian RL in POMDPs                   | Jason, Devin               |

### Links:

- [Tax Collection Optimization](https://pubsonline.informs.org/doi/abs/10.1287/inte.1110.0618)
- [Model Predictive Control and RL](https://www.sciencedirect.com/science/article/pii/S2405896317311941)
- [Thompson Sampling](https://arxiv.org/abs/1301.2609) 
- [Optimizer's curse](https://pdfs.semanticscholar.org/ce61/396a4abe1da265f4b5d3f05d11fd206c40f7.pdf)
- [Biasing with a Discount Factor](http://marek.petrik.us/pub/Petrik2009a.pdf)
- [Bayesian RL in POMDPs](https://www.cs.cmu.edu/~sross1/publications/gppomgp_IROS09.pdf)
- [RL for Recommendations](https://arxiv.org/abs/1812.10613)


## Grading ##

- Topic (1x) & paper presentation (2x)  (40%)
- Project  (40%)
- Homework (~20) (pass-fail each problem) (20%) (4-worst ones do not count)

## Presenting a Textbook Topic ##

Each student is expected to present 2 topics and each topic will be covered by 3 students. The presentation should not be just a rehash of the book, but should focus on:

1. Brief summary of the topic
2. Solutions to the homework problems
3. Any additional related topic

## Presenting a Paper ##

Every student will be expected to present 1 paper and each paper will be presented by 2 students. Please review the following helpful guide on how to review read research papers. [How to read a research paper](docs/efficientReading.pdf)

When presenting a paper or writing a response to it, you should address the following points:

1. *Motivation*: Why is this problem important?
2. *Challenge*: Why is it difficult?
3. *Approach*: What is the main approach?
4. *Novelty*: What is new about the paper?
5. *Evidence*: Is there sufficient evidence of the effectiveness of the methods?
6. *Limitations*: What is it that the paper does not address? Are there alternative methods that can work?

Please prepare slides for the presentation that include relevant figures or tables from the paper.

#### Paper Assignments

TBD. We will have a poll to choose which papers to read. Good places to look for papers are: ICML, NeurIPS/NIPS, JMLR, JAIR, Operations Reseach, Math of Operations Research, IEEE TAC, EJOR

## Writing a Paper Response ##

For every paper (column reading says: "paper"), every student is expected to write a short paper response. The response should consist of:

1. A paragraph describing the main idea of the paper
2. 2-4 paragraphs addressing the points mentioned in the section "Presenting a Paper"

## Project ##

The project is a group project with group size of 1 - 15. I will offer several options or you may choose a topic that interests you.

# Sources #

## Books ##

### RL ###
- [ARL]: Szepesvári, C. (2010). [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html). 
- [RL]: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://incompleteideas.net/book/the-book-2nd.html). MIT Press. **2nd Edition**

### MDPs ###
- [MDP]: Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. John Wiley & Sons, Inc.

### Probability and Statistics ###
- Grinstead, C., & Snell, J. (1998). [Introduction to probability](https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/amsbook.mac.pdf). 
- Lavine, M. (2005). [Introduction to statistical thought](http://people.math.umass.edu/~lavine/Book/book.html).

### Advanced ML ###
- [ALML]: [Advanced Lectures on Machine Learning](https://link.springer.com/book/10.1007/b100712)

### Other ###
- [NW]: [Newsvendor Notes](http://courses.ieor.berkeley.edu/ieor151/lecture_notes/ieor151_lec5.pdf)
